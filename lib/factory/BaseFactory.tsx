import { TFieldValidation, FieldFactory } from './FieldFactory'
import { AnyObjectSchema } from 'yup'
import * as Yup from 'yup'
import { Control, Controller } from 'react-hook-form'

export type Factory<K extends string> = Record<K, FieldFactory>
export type DependentKeysType = [string, string][]

export class BaseFactory<T extends string> {
  addCommonStyle(style: Record<string, unknown>): void {
    Object.values(this).forEach(
      (value) => value instanceof FieldFactory && value.addProps({ style }),
    )
  }

  getFieldsValidation(fields?: T[]): TFieldValidation[] {
    return Object.entries(this)
      .filter(
        ([key, value]) =>
          value instanceof FieldFactory &&
          (fields ? fields.includes(key as T) : true),
      )
      .map(([, value]) => value.validation)
  }

  getFields<Fields extends T>(
    fields?: Fields[],
    excludeFields?: Fields[],
  ): Factory<Fields> {
    return Object.entries(this)
      .filter(
        ([key, value]) =>
          value instanceof FieldFactory &&
          (fields ? fields.includes(key as Fields) : true) &&
          (excludeFields ? !excludeFields.includes(key as Fields) : true),
      )
      .reduce((accumulator, [key, value]) => {
        accumulator[key as string] = value
        return accumulator
      }, {} as Factory<T>)
  }

  createYupValidationSchema(
    fields: FieldFactory[],
    dependentKeys?: DependentKeysType,
  ): AnyObjectSchema {
    return Yup.object().shape(
      fields
        .filter((field) => !field.hidden)
        .reduce((accumulator, current, _, array) => {
          const name = current.getName()
          /**
           * The following logic will create a nested array of object validation schema
           * based on the field name
           */
          const nameSplit = name.split('.')
          if (nameSplit.length >= 3) {
            const [parent, index] = nameSplit
            if (!accumulator[parent as string]) {
              accumulator[parent as string] = Yup.array().of(
                Yup.object().shape(
                  array
                    .filter((field) =>
                      field.getName().includes(`${parent}.${index}`),
                    )
                    .reduce(
                      (nestedAcc: Record<string, any>, field) => ({
                        ...nestedAcc,
                        [field.getName().split('.').pop() || '']:
                          field.validation,
                      }),
                      {},
                    ),
                ),
              )
            }
          } else accumulator[current.getName()] = current.validation
          return accumulator
        }, {}),
      dependentKeys,
    )
  }

  renderReactHookForm(fields: FieldFactory[], control: Control) {
    return (
      <>
        {fields.map((field) => (
          <Controller
            key={field.getName()}
            control={control}
            name={field.getName()}
            render={({
              field: { value, onBlur, onChange },
              fieldState: { error },
            }) => field.render({ value: value || '', onBlur, onChange, error })}
          />
        ))}
      </>
    )
  }
}
