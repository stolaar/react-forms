import { Context, createContext, ReactNode } from 'react'
import { BaseFactory } from './BaseFactory'
import { FormFactoryMethods, useFormFactory } from '../hooks/useFormFactory'

export type ContextReturnType<Fields extends string> =
  FormFactoryMethods<Fields>

export const createFormContext = <Fields extends string>() => {
  return createContext<ContextReturnType<Fields> | null>(null)
}

export interface IFormProviderProps<
  TFactory extends typeof BaseFactory<Fields>,
  Fields extends string = '',
> {
  children: ReactNode
  fieldsFactory: TFactory
  withFields?: Fields[]
  excludeFields?: Fields[]
  Context: Context<ContextReturnType<Fields | null>>
}

export const FormContextProvider = <
  TFactory extends typeof BaseFactory<Fields>,
  Fields extends string = '',
>({
  children,
  fieldsFactory,
  withFields,
  excludeFields,
  Context,
}: IFormProviderProps<TFactory, Fields>): JSX.Element => {
  const factoryMethods = useFormFactory<TFactory, Fields>(fieldsFactory, {
    formProps: {
      mode: 'onChange',
      criteriaMode: 'all',
    },
    withFields,
    excludeFields,
  })
  return <Context.Provider value={factoryMethods}>{children}</Context.Provider>
}
