import { FC } from 'react'
import { AnySchema } from 'yup'

export type TFieldValidation = any | AnySchema

export interface BaseProps {
  name?: string
}

export class FieldFactory<Props extends BaseProps = Record<string, unknown>> {
  component: FC<Props>

  validation: TFieldValidation

  initialValidation: TFieldValidation

  props: Props = {} as Record<string, unknown> as Props

  hidden = false

  constructor(
    component: FC<Props>,
    validation: TFieldValidation,
    defaultProps?: Props,
  ) {
    this.component = component
    this.validation = validation
    this.initialValidation = validation
    if (defaultProps) this.props = defaultProps
  }

  render<T = Props>(props?: Partial<T>) {
    if (this.hidden) return null
    const combinedProps = { ...this.props, ...props }
    const Component = this.component
    return <Component {...combinedProps} />
  }

  addProps<AddPropsType = Props>(props: Partial<AddPropsType>): this {
    this.props = { ...this.props, ...props }
    return this
  }

  withComponent(component: FC): this {
    this.component = component
    return this
  }

  getName(): string {
    return this.props?.name || ''
  }

  addValidation(validation: TFieldValidation, override = false): this {
    this.validation = override
      ? validation
      : { ...this.validation, ...validation }
    return this
  }

  hide(shouldHide: boolean): this {
    this.hidden = shouldHide
    return this
  }
}
