import { FieldFactory } from '../factory/FieldFactory'

export const preventFieldDuplicates = (fields: FieldFactory[]) => {
  return fields.reduce((accumulator, current) => {
    if (accumulator.some((field) => field.getName() === current.getName()))
      return accumulator

    accumulator.push(current)
    return accumulator
  }, [])
}
