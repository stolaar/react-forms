import { FC } from 'react'
import { FieldFactory } from '../factory/FieldFactory'

export interface IFormFieldProps {
  field: FieldFactory
}

export const FormField: FC<IFormFieldProps> = ({
  field,
}): JSX.Element | null => {
  return field.render()
}
