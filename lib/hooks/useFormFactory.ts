import { useCallback, useEffect, useMemo, useState } from 'react'
import { DependentKeysType, BaseFactory, Factory } from '../factory/BaseFactory'
import { AnyObjectSchema } from 'yup'
import { useForm, UseFormProps, UseFormReturn } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { FieldFactory } from '../factory/FieldFactory'
import { preventFieldDuplicates } from '../utils/preventFieldDuplicates'

export type FormFactoryMethods<Fields extends string> = UseFormReturn & {
  renderForm: (updatedFields?: FieldFactory[]) => JSX.Element
  factory: Factory<Fields> & BaseFactory<Fields>
  fields: Factory<Fields>
  schema: AnyObjectSchema | null
  setValidationSchema: (
    fields: FieldFactory[],
    updateDependentKeys?: DependentKeysType,
  ) => void
}

export interface FormFactoryOptions<Fields> {
  formProps?: UseFormProps
  withFields?: Fields[]
  dependentKeys?: DependentKeysType
  excludeFields?: Fields[]
}

export const useFormFactory = <
  TFactory extends typeof BaseFactory<Fields>,
  Fields extends string,
>(
  FieldsFactory: TFactory,
  options: FormFactoryOptions<Fields>,
): FormFactoryMethods<Fields> => {
  const { formProps, dependentKeys, withFields, excludeFields } = options
  const factory = useMemo(() => new FieldsFactory(), []) as Factory<Fields> &
    BaseFactory<Fields>
  const [schema, setSchema] = useState<AnyObjectSchema | null>(null)

  const resolver = useMemo(() => schema && yupResolver(schema), [schema])
  const { control, ...formMethods } = useForm({ ...formProps, resolver })

  const getRequestedFields = useCallback(
    () => factory && factory.getFields(withFields, excludeFields),
    [factory, withFields, excludeFields],
  )

  const fields = useMemo(() => getRequestedFields(), [getRequestedFields])

  useEffect(() => {
    setSchema(
      factory.createYupValidationSchema(Object.values(fields), dependentKeys),
    )
  }, [factory, fields, dependentKeys])

  const setValidationSchema = useCallback(
    (fields: FieldFactory[], updateDependentKeys?: DependentKeysType) => {
      setSchema(
        factory.createYupValidationSchema(
          fields,
          updateDependentKeys || dependentKeys,
        ),
      )
    },
    [factory, dependentKeys],
  )

  const renderForm = useCallback(
    (updatedFields?: FieldFactory[]) => {
      const currentFieldsArray: FieldFactory[] = Object.values(fields)
      return factory.renderReactHookForm(
        updatedFields
          ? preventFieldDuplicates([...updatedFields, ...currentFieldsArray])
          : Object.values(fields),
        control,
      )
    },
    [fields, control],
  )

  return {
    fields,
    factory,
    schema,
    setValidationSchema,
    renderForm,
    control,
    ...formMethods,
  }
}
