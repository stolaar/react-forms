import { UserFieldKeys } from '../../form-fields/user/UserFields'
import { useStudentFields } from '../../form-fields/student/StudentContextProvider'

export const StudentDetailsForm = () => {
  const {
    renderForm,
    handleSubmit,
    formState: { isValid, isDirty },
  } = useStudentFields<UserFieldKeys>()

  const onSubmit = (values: any) => {
    console.log('Submit values', values)
  }

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        {renderForm()}
        <button type={'submit'} disabled={!isDirty || !isValid}>
          Submit
        </button>
      </form>
    </div>
  )
}
