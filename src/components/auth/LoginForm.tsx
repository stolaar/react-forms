import { useAuthFields } from '../../form-fields/user/UserContextProvider'
import { UserFieldKeys } from '../../form-fields/user/UserFields'
import { useEffect } from 'react'

export const LoginForm = () => {
  const {
    factory,
    renderForm,
    handleSubmit,
    fields,
    formState: { isValid, isDirty },
  } = useAuthFields<
    Extract<UserFieldKeys, UserFieldKeys.email | UserFieldKeys.password>
  >()

  useEffect(() => {
    factory.addCommonStyle({ borderRadius: '10px' })
  }, [factory])

  const onSubmit = (values: { email: string; password: string }) => {
    console.log('Submit values', values)
  }

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        {renderForm([fields.email.addProps({ style: { borderRadius: '0' } })])}
        <button type={'submit'} disabled={!isDirty || !isValid}>
          Submit
        </button>
      </form>
    </div>
  )
}
