import { useAuthFields } from '../../form-fields/user/UserContextProvider'
import { UserFieldKeys } from '../../form-fields/user/UserFields'

export const RegisterForm = () => {
  const {
    renderForm,
    handleSubmit,
    formState: { isValid, isDirty },
  } = useAuthFields<UserFieldKeys>()

  const onSubmit = (values: any) => {
    console.log('Submit values', values)
  }

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        {renderForm()}
        <button type={'submit'} disabled={!isDirty || !isValid}>
          Submit
        </button>
      </form>
    </div>
  )
}
