import React, { FC } from 'react'

export interface ICardProps {
  name: string
  value: string
}

export const Card: FC<ICardProps> = ({ name, value }): JSX.Element => {
  return (
    <div>
      {name} {value}
    </div>
  )
}
