import React, { DetailedHTMLProps, FC } from 'react'

export type TextInputProps = Partial<
  DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >
> & {
  error?: { message: string }
}

export const TextInput: FC<TextInputProps> = (props): JSX.Element => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <input type="text" {...props} />
      {props.error ? (
        <span style={{ color: 'red' }}>{props.error.message}</span>
      ) : null}
    </div>
  )
}
