import { IFormProviderProps } from '../../lib/factory/FormContextFactory'
import { BaseFactory } from '../../lib/factory/BaseFactory'

export type SpecificFormContextProviderProps<
  TFactory extends typeof BaseFactory<Fields>,
  Fields extends string = '',
> = Omit<IFormProviderProps<TFactory, Fields>, 'fieldsFactory' | 'Context'>
