import {
  createFormContext,
  ContextReturnType,
  FormContextProvider,
} from '../../../lib/factory/FormContextFactory'
import { UserFieldKeys, UserFields } from './UserFields'
import { FC, useContext } from 'react'
import { SpecificFormContextProviderProps } from '../../types'

export const UserContext = createFormContext<UserFieldKeys>()

export const useAuthFields = <Fields extends string>() =>
  useContext<ContextReturnType<Extract<UserFieldKeys, Fields>>>(UserContext)

export const UserContextProvider: FC<
  SpecificFormContextProviderProps<typeof UserFields, UserFieldKeys>
> = ({ children, ...rest }) => (
  <FormContextProvider
    fieldsFactory={UserFields}
    Context={UserContext}
    {...rest}
  >
    {children}
  </FormContextProvider>
)
