import { BaseFactory, Factory } from '../../../lib/factory/BaseFactory'
import { FieldFactory } from '../../../lib/factory/FieldFactory'
import { TextInput, TextInputProps } from '../../components/TextInput'
import * as Yup from 'yup'

export enum UserFieldKeys {
  email = 'email',
  password = 'password',
  password2 = 'password2',
  firstName = 'firstName',
  lastName = 'lastName',
}

export class UserFields
  extends BaseFactory<UserFieldKeys>
  implements Factory<UserFieldKeys>
{
  email = new FieldFactory<Partial<TextInputProps>>(
    TextInput,
    Yup.string().email('Invalid email'),
    { name: UserFieldKeys.email, placeholder: 'Email' },
  )

  password = new FieldFactory<Partial<TextInputProps>>(
    TextInput,
    Yup.string().min(6),
    { name: UserFieldKeys.password, type: 'password', placeholder: 'Password' },
  )

  password2 = new FieldFactory<Partial<TextInputProps>>(
    TextInput,
    Yup.string().oneOf(
      [Yup.ref(UserFieldKeys.password), null],
      "Passwords doesn't match",
    ),
    {
      name: UserFieldKeys.password2,
      type: 'password',
      placeholder: 'Confirm password',
    },
  )

  firstName = new FieldFactory<Partial<TextInputProps>>(
    TextInput,
    Yup.string().min(3),
    { name: UserFieldKeys.firstName, placeholder: 'First name' },
  )

  lastName = new FieldFactory<Partial<TextInputProps>>(
    TextInput,
    Yup.string().min(3),
    { name: UserFieldKeys.lastName, placeholder: 'Last Name' },
  )

  constructor() {
    super()
  }
}
