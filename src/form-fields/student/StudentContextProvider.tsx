import {
  createFormContext,
  ContextReturnType,
  FormContextProvider,
} from '../../../lib/factory/FormContextFactory'
import { FC, useContext } from 'react'
import { SpecificFormContextProviderProps } from '../../types'
import { StudentFieldKeys, StudentFields } from './StudentFields'
import { UserFieldKeys } from '../user/UserFields'

type TStudentUserKeys = StudentFieldKeys & UserFieldKeys

const StudentUserKeys: string = {
  ...StudentFieldKeys,
  ...UserFieldKeys,
} as TStudentUserKeys

export const StudentContext = createFormContext<typeof StudentUserKeys>()

export const useStudentFields = <Fields extends string>() =>
  useContext<ContextReturnType<Extract<typeof StudentUserKeys, Fields>>>(
    StudentContext,
  )

export const StudentContextProvider: FC<
  SpecificFormContextProviderProps<typeof StudentFields, typeof StudentUserKeys>
> = ({ children, ...rest }) => (
  <FormContextProvider
    fieldsFactory={StudentFields}
    Context={StudentContext}
    {...rest}
  >
    {children}
  </FormContextProvider>
)
