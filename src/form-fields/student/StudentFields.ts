import { UserFields } from '../user/UserFields'
import { Factory } from '../../../lib/factory/BaseFactory'
import { FieldFactory } from '../../../lib/factory/FieldFactory'
import { TextInput } from '../../components/TextInput'
import * as Yup from 'yup'

export enum StudentFieldKeys {
  subject = 'subject',
}

export class StudentFields
  extends UserFields
  implements Factory<StudentFieldKeys>
{
  subject = new FieldFactory(TextInput, Yup.string().required(), {
    name: StudentFieldKeys.subject,
    placeholder: 'Subject',
  })

  constructor() {
    super()
  }
}
