import type { NextPage } from 'next'
import styles from '../../styles/Home.module.css'
import { UserFieldKeys } from '../form-fields/user/UserFields'
import { StudentDetailsForm } from '../components/profile/StudentDetailsForm'
import { StudentContextProvider } from '../form-fields/student/StudentContextProvider'

const StudentDetails: NextPage = () => {
  return (
    <main className={styles.main}>
      <h6 className={styles.title}>Student Details</h6>
      <StudentContextProvider
        excludeFields={[UserFieldKeys.password, UserFieldKeys.password2]}
      >
        <StudentDetailsForm />
      </StudentContextProvider>
    </main>
  )
}

export default StudentDetails
