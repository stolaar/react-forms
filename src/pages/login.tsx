import type { NextPage } from 'next'
import styles from '../../styles/Home.module.css'
import { UserContextProvider } from '../form-fields/user/UserContextProvider'
import { LoginForm } from '../components/auth/LoginForm'
import { UserFieldKeys } from '../form-fields/user/UserFields'

const Login: NextPage = () => {
  return (
    <main className={styles.main}>
      <h6 className={styles.title}>Welcome to Login page</h6>
      <UserContextProvider
        withFields={[UserFieldKeys.email, UserFieldKeys.password]}
      >
        <LoginForm />
      </UserContextProvider>
    </main>
  )
}

export default Login
