import type { NextPage } from 'next'
import styles from '../../styles/Home.module.css'
import { UserContextProvider } from '../form-fields/user/UserContextProvider'
import { UserFieldKeys } from '../form-fields/user/UserFields'
import { RegisterForm } from '../components/auth/RegisterForm'

const Register: NextPage = () => {
  return (
    <main className={styles.main}>
      <h6 className={styles.title}>Welcome to Login page</h6>
      <UserContextProvider
        withFields={[
          UserFieldKeys.email,
          UserFieldKeys.password,
          UserFieldKeys.password2,
        ]}
      >
        <RegisterForm />
      </UserContextProvider>
    </main>
  )
}

export default Register
