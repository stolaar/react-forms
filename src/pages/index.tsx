import type { NextPage } from 'next'
import styles from '../../styles/Home.module.css'

const Home: NextPage = () => {
  return (
    <main className={styles.main}>
      <h6 className={styles.title}>
        Welcome to <a href="https://nextjs.org">Next.js!</a>
      </h6>
      <a href={'/login'}>Login</a>
      <a href={'/register'}>Register</a>
      <a href={'/profile-details'}>Profile details</a>
      <a href={'/student-details'}>Student details</a>
    </main>
  )
}

export default Home
